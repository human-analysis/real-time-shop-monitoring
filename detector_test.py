from libs.detection.detector import Detector
from glob import glob
import os
import time
import torch
import cv2


if __name__ == '__main__':
    detector = Detector(detector_name='darknet19_fssd', config_path='storage/config.cfg')
    # image_samples = glob(os.path.join('./storage/sample/images', '*NVL601*.jpg'))
    image_samples = glob(os.path.join('./storage/sample/', 'considering_image.jpg'))
    # print(len(image_samples))
    total_time = 0
    # for img_path in image_samples[:1]:
    for i in range(10):
        start = time.time()
        results = detector.detect_image('./storage/sample/considering_image.jpg', is_display=True)
        total_time = total_time + time.time() - start
        if detector.next_display is False:
            break


    print(total_time / len(image_samples))