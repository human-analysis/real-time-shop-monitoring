# -*- coding: utf-8 -*-
"""
Created on Sun Jun 5 14:00:00 2019

@author: Tuanhleo
"""

import configparser
import pickle

import cv2
from PIL import Image

from libs.utils.box_preprocessing import counting_box_processing, monitoring_box_processing
from libs.utils.hog_utils import get_concat_features, filering_processing
from .detector_template import Detector_Template
from .models.fssd import FSSD
from .models.mobilenet_ssd import Mobilenet_Ssd


def _detector_selection(detector_name, config_path):
    """
    :param detector_name:
    :param config_path: may be remove this param.
    :return:
    """
    detector_map = {
        'darknet19_fssd': FSSD,
        'darknet19_fssd_shop': FSSD,
        'mobilenetv1_fssd': FSSD,
        'mobilenetv1_ssd': Mobilenet_Ssd
    }

    return detector_map[detector_name](config_path, detector_name)


def _processor_selection(processor_name):
    processor_map = {
        'counting': counting_box_processing,
        'monitoring': monitoring_box_processing,
    }
    return processor_map[processor_name]


class Detector(Detector_Template):
    def __init__(self, detector_name, config_path):
        super(Detector_Template, self).__init__()
        self.detector_name = detector_name
        self.detector = _detector_selection(detector_name, config_path)

        config = configparser.ConfigParser()
        config.read(config_path)
        self.detect_frequency = int(config.get('common_config', 'detect_frequency'))
        self.is_display = config.get('common_config', 'is_display') == 'True'
        self.confident_threshold = float(config.get('common_config', 'confident_threshold'))
        self.is_filter = config.get('common_config', 'is_filter') == 'True'
        self.scaling_model_path = config.get('common_config', 'scaling_model')
        self.hog_model_path = config.get('common_config', 'hog_model')
        # self.box_processor = _processor_selection(config.get(''))

        self.scaler, self.hog_model = None, None
        if self.is_filter:
            with open(self.scaling_model_path, 'rb') as fid:
                self.scaler = pickle.load(fid)
            with open(self.hog_model_path, 'rb') as fid:
                self.hog_model = pickle.load(fid)
        self.next_display = True

    def _detect_image(self, image_frame, to_xywh, ratio_filter=1.0):
        height, width = image_frame.shape[:2]
        if 'yolo' in self.detector_name:
            image_frame = Image.fromarray(image_frame)
        detection_results = self.detector.detect_image(image_frame, height, width,
                                                       to_xywh, self.confident_threshold, ratio_filter)
        if self.is_filter:
            detection_results, scores = self.result_filtering(image_frame, detection_results)
        # for i in range(len(scores)):
        #     print(detection_results[i][-1], scores[i])
        return detection_results

    def detect_image_frame(self, image_frame, to_xywh, ratio_filter):
        detection_results = self._detect_image(image_frame, to_xywh, ratio_filter)
        ret_results = list()
        for detection_result in detection_results:
            ret_results.append(detection_result)
        return ret_results

    def detect_image(self, image_path, filtering_threshold=0.14,
                     hog_threshold=0.57, to_xywh=False, is_display=True):
        image_frame = cv2.imread(image_path)
        if 'yolo' in self.detector_name:
            h, w, _ = image_frame.shape
            image_frame = cv2.resize(image_frame, (w, h))
        detection_results = self._detect_image(image_frame, to_xywh)

        if is_display:
            _display(image_frame, detection_results)
            cv2.imshow('image', image_frame)

            k = cv2.waitKey()
            if k == ord('n'):
                cv2.destroyAllWindows()
            elif k == ord('q'):
                self.next_display = False
            # from google.colab.patches import cv2_imshow
            # cv2_imshow(image_frame)

        return detection_results

    def result_filtering(self, image, detection_results, _threshold=0.30, hog_threshold=0.535):
        filtering_results = []
        percetage = []
        for result in detection_results:
            if result[5] > _threshold:
                filtering_results.append(result)
                percetage.append([0.0, 1.0])
                continue
            subimg = image[int(result[2]):int(result[4]), int(result[1]):int(result[3]), :]
            subimg = filering_processing(subimg)
            final_features = get_concat_features(subimg)
            scaled_features = self.scaler.transform(final_features)
            human_prob = self.hog_model.predict_proba(scaled_features)
            if human_prob[0][1] > 0.96 and result[5] < 0.155:
                continue
            print(result[5], human_prob)
            if human_prob[0][1] > hog_threshold:
                filtering_results.append(result)
                percetage.append(human_prob[0])

        return filtering_results, percetage


def _display(image_frame, results):
    for result in results:
        confidence = result[5]
        # label = "{}: {:.2f}%".format(result[0], confidence * 100)
        label = "{:.4f}%".format(confidence * 100)
        cv2.rectangle(image_frame, (int(result[1]), int(result[2])), (int(result[3]), int(result[4])), (255, 0, 0),
                      1)
        y = result[2] - 15 if result[2] - 15 > 15 else result[2] + 15
        cv2.putText(image_frame, label, (result[1], y + 10), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (0, 0, 255), 1)
