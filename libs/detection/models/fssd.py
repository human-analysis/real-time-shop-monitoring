import configparser

from libs.utils.fssd_utils.config_parse import cfg_from_file, cfg
from libs.utils.geometry_common import orientation
from .inference import Detector


class FSSD(object):
    def __init__(self, config_path, net_model):
        config = configparser.ConfigParser()
        config.read(config_path)
        cfg_from_file(config.get(net_model, 'config_model'))
        self.image_width = int(config.get(net_model, 'image_width'))
        self.image_height = int(config.get(net_model, 'image_height'))
        self.detect_classes = config.get(net_model, 'detect_classes').split(',')
        self.ignore_classes = set(config.get(net_model, 'ignore_classes').split(','))
        self.detector = Detector(cfg, config.get(net_model, 'is_model_showing') == 'True')
        self.model_version = config.get(net_model, 'version')

    def box_filtering(self, box, p1=[356, 116], p2=[632, 43]):
        """
        :param box:[ret1, ret2, ret3, ret4]
        :param p1:
        :param p2:
        :return:
        """
        if box[0] > p1[0] and box[1] < p1[1]:
            if orientation(p1, p2, [box[0], box[1]]) == 1:
                return True
        return False

    def detect_image(self, image_frame, height, width, to_xywh, confident_threshold,
                     ratio_filter):
        _labels, _scores, _coords, times = self.detector.predict(image_frame,
                                                                 confident_threshold, check_time=True)
        # print(times)
        detection_results = []
        for label, score, coord in zip(_labels, _scores, _coords):
            if (self.detect_classes[label + 1] in self.ignore_classes):
                continue
            else:
                ret1, ret2, ret3, ret4 = int(coord[0]), int(coord[1]), int(coord[2]), int(coord[3])
                # if statifying_checking([[ret1, ret2], [ret3, ret2]]):
                #     continue # for alarm demo
                # for counting:
                if self.box_filtering([ret1, ret2, ret3, ret4]):
                    continue
                w, h = ret3 - ret1, ret4 - ret2
                if ratio_filter != 1.0:
                    if h > w * ratio_filter:
                        ret3 = ret1 + w
                        ret4 = ret2 + w * ratio_filter
                if w > 200:
                    continue
                if to_xywh is True:
                    ret1, ret2, ret3, ret4 = int(ret1), int(ret2), int(ret3 - ret1), int(ret4 - ret2)

                    if ret1 < 0:
                        ret3 += ret1
                        ret1 = 0
                    if ret2 < 0:
                        ret4 += ret2
                        ret2 = 0

                detection_results.append([self.detect_classes[label + 1], ret1, ret2, ret3, ret4, score])
        return detection_results
