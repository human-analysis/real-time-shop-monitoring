import torch
from torch.autograd import Variable

from libs.detection.models.fssd_modeling.layers.functions.detection import Detect
from libs.utils.dataset import preproc
from libs.utils.timer import Timer
from .fssd_modeling import create_model

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class Detector():
    def __init__(self, cfg, viz_arch=False):
        self.cfg = cfg

        # Build model
        self.model, self.priorbox = create_model(cfg.MODEL)
        with torch.no_grad():
            self.priors = Variable(self.priorbox.forward())

        if viz_arch is True:
            print('Model architectures:\n{}\n'.format(self.model()))

        # Utilize GPUS for computation
        self.use_gpu = torch.cuda.is_available()
        self.half = False
        if self.use_gpu:
            print('Utilize GPUs for computation')
            print('Number of GPU avaiable', torch.cuda.device_count())
            self.model = self.model.to(device)
            self.priors = self.priors.to(device)
            # cudnn.benchmark = True
            # Utilize half precision
            self.half = cfg.MODEL.HALF_PRECISION
            if self.half:
                self.model = self.model.half()
                self.priors = self.priors.half()

            # Build preprocessor and detection
        self.preprocessor = preproc(cfg.MODEL.IMAGE_SIZE, cfg.DATASET.PIXEL_MEANS, -2)
        self.detector = Detect(cfg.POST_PROCESS, self.priors)

        # Load weight:
        if cfg.RESUME_CHECKPOINT == '':
            AssertionError('RESUME_CHECKPOINT can not be empty')
        print('=> loading checkpoint {:s}'.format(cfg.RESUME_CHECKPOINT))
        if self.use_gpu:
            checkpoint = torch.load(cfg.RESUME_CHECKPOINT)
        else:
            checkpoint = torch.load(cfg.RESUME_CHECKPOINT, map_location='cpu')
        self.model.load_state_dict(checkpoint)
        # test only
        self.model.eval()

    # def box_filtering(self, p1=[356, 116], p2=[632, 43]):

    def predict(self, img, threshold=0.6, check_time=False):
        # make sure the input channel is 3
        assert img.shape[2] == 3
        scale = torch.Tensor([img.shape[1], img.shape[0], img.shape[1], img.shape[0]])

        _t = {'preprocess': Timer(), 'net_forward': Timer(), 'detect': Timer(), 'output': Timer()}

        # preprocess image
        _t['preprocess'].tic()
        with torch.no_grad():
            x = Variable(self.preprocessor(img)[0].unsqueeze(0))
        if self.use_gpu:
            x = x.to(device)
        if self.half:
            x = x.half()
        preprocess_time = _t['preprocess'].toc()

        # forward
        _t['net_forward'].tic()
        out = self.model(x)  # forward pass
        net_forward_time = _t['net_forward'].toc()

        # detect
        _t['detect'].tic()
        detections = self.detector.forward(out)
        detect_time = _t['detect'].toc()

        # output
        _t['output'].tic()
        labels, scores, coords = [list() for _ in range(3)]
        # for batch in range(detections.size(0)):
        #     print('Batch:', batch)
        batch = 0
        for classes in range(detections.size(1)):
            num = 0
            while detections[batch, classes, num, 0] >= threshold:
                scores.append(detections[batch, classes, num, 0])
                labels.append(classes - 1)
                coords.append(detections[batch, classes, num, 1:] * scale)
                num += 1
        output_time = _t['output'].toc()
        total_time = preprocess_time + net_forward_time + detect_time + output_time

        if check_time is True:
            return labels, scores, coords, (total_time, preprocess_time, net_forward_time, detect_time, output_time)
            # total_time = preprocess_time + net_forward_time + detect_time + output_time
            # print('total time: {} \n preprocess: {} \n net_forward: {} \n detect: {} \n output: {}'.format(
            #     total_time, preprocess_time, net_forward_time, detect_time, output_time
            # ))
        return labels, scores, coords
