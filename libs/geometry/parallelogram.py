def rec_intersect(line_segment, rectangle):
    intersects = []
    x1, y1 = rectangle[0][0], rectangle[0][1]
    x2, y2 = rectangle[1][0], rectangle[1][1]
    if (line_segment[0][0] <= x1 and x1 <= line_segment[1][0]) and (
            line_segment[0][0] <= x2 and x2 <= line_segment[1][0]):
        rectangle_side = [rectangle[0], rectangle[1]]
        slope = (line_segment[1][1] - line_segment[0][1]) / (line_segment[1][0] - line_segment[0][0])
        y_temp = slope * (rectangle_side[0][0] - line_segment[0][0]) + line_segment[0][1]
        if abs(y_temp - rectangle_side[0][1]) < 1:
            return True
        # for i in range(len(rectangle)):
        #     rectangle_side = [rectangle[i], rectangle[(i+1)%(len(rectangle))]]
        #     slope = (line_segment[1][1] - line_segment[0][1]) / (line_segment[1][0] - line_segment[0][0])
        #     if do_intersect(rectangle_side[0], rectangle_side[1], line_segment[0], line_segment[1]):
        #         if rectangle_side[0][0] == rectangle_side[1][0]:
        #             intersect = [rectangle_side[0][0] / 1.0,
        #                     slope *(rectangle_side[0][0] - line_segment[0][0]) + line_segment[0][1]]
        #             if intersect not in intersects:
        #                 intersects.append(intersect)
        #         else:
        #             intersect = [(rectangle_side[0][1] - line_segment[0][1]) / slope + line_segment[0][0],
        #                                rectangle_side[0][1]/1.0]
        #             if intersect not in intersects:
        #                 intersects.append(intersect)
        #
        # return intersects
    return False


def same_check(poin1, poin2):
    if abs(poin1[0] - poin2[0]) < 80 and abs(poin1[1] - poin2[1]) < 80:
        return True
    return False


def statifying_checking(sg1, sg2s=[[[961, 195], [1034, 195]], [[1202, 350], [1272, 350]]]):
    if (same_check(sg1[0], sg2s[0][0]) and same_check(sg1[1], sg2s[0][1])) \
            or (same_check(sg1[0], sg2s[1][0]) and same_check(sg1[1], sg2s[1][1])):
        return True
    return False


if __name__ == "__main__":
    print(statifying_checking([[961, 195], [1034, 195]]))
    # print(do_intersect([1, 1], [1, 4], [0, 1], [4, 5]))
