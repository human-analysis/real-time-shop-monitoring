import cv2

from libs.utils.geometry_common import is_inside_polygon

CANVAS_SIZE = (600, 800)
FINAL_LINE_COLOR = (255, 255, 255)
WORKING_LINE_COLOR = (127, 127, 127)


class PolygonDrawer(object):

    def __init__(self, window_name, is_test=True):
        self.window_name = window_name
        self.done = False
        self.points = []

    def on_mouse(self, event, x, y, buttons, user_params):
        if self.done:
            return

        if event == cv2.EVENT_LBUTTONDOWN:
            self.points.append((x, y))
        elif event == cv2.EVENT_RBUTTONDOWN:
            self.done = True

    def run(self, image):
        cv2.namedWindow(self.window_name)
        cv2.imshow(self.window_name, image)
        cv2.waitKey(1)
        cv2.setMouseCallback(self.window_name, self.on_mouse)
        canvas = image

        while (not self.done):
            # This is our drawing loop, we just continuously draw new images
            # and show them in the named window
            if (len(self.points) > 0):
                # Draw all the current polygon segments
                # cv2.polylines(canvas, np.array([self.points]), True, FINAL_LINE_COLOR, 1)
                if len(self.points) >= 2:
                    cv2.line(canvas, (int(self.points[-1][0]), int(self.points[-1][1])),
                             (int(self.points[-2][0]), int(self.points[-2][1])), (0, 255, 0), 2)
                # And  also show what the current segment would look like
            # Update the window
            cv2.imshow(self.window_name, canvas)
            # And wait 50ms before next iteration (this will pump window messages meanwhile)
            if cv2.waitKey(50) == 27:  # ESC hit
                cv2.line(canvas, (int(self.points[-1][0]), int(self.points[-1][1])),
                         (int(self.points[0][0]), int(self.points[0][1])), (0, 255, 0), 2)
                self.done = True

            # User finised entering the polygon points, so let's make the final drawing
        # of a filled polygon
        # if (len(self.points) > 0):
        #     cv2.fillPoly(canvas, np.array([self.points]), FINAL_LINE_COLOR)
        # And show it
        # cv2.imshow(self.window_name, canvas)
        # # Waiting for the user to press any key
        # cv2.waitKey()
        #
        # cv2.destroyWindow(self.window_name)
        return canvas

    def pointInPolygon(self, canvas, point, is_display=False):
        cv2.circle(canvas, (int(point[0]), int(point[1])), 4, (0, 255, 0), -1)
        text = "Out"
        if is_inside_polygon(self.points, len(self.points), point) == 1:
            text = "In"
        cv2.putText(canvas, text, (0, 100), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

        cv2.imshow(self.window_name, canvas)
        # Waiting for the user to press any key
        cv2.waitKey()
        cv2.destroyWindow(self.window_name)
        return canvas
