import colorsys
import datetime
import time
import warnings

import cv2
import numpy as np
from matplotlib.pylab import cm

from libs.detection.detector import Detector
from libs.geometry.polygon import PolygonDrawer
from libs.tracking.tracker import Tracker_temp
from libs.utils.common import readpoints, savefile
from libs.utils.geometry_common import is_inside_polygon

warnings.filterwarnings('ignore')
IS_DETECTION_DISPLAY = False
IS_TRACKING_DISPLAY = True
points = []


def draw_poly():
    pass


hsv_tuples = [(x / 80, 1., 1.)
              for x in range(80)]
colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
colors = list(
    map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)), colors))


def get_crop_size(cap, down_sample_ratio):
    global points
    pd = PolygonDrawer("draw polygon")
    while cap.isOpened():
        ret, frame = cap.read()
        (h, w) = frame.shape[:2]
        frame = cv2.resize(frame, (int(w * down_sample_ratio), int(h * down_sample_ratio)))
        frame = pd.run(frame)
        points = pd.points
        while 1:
            cv2.imshow('draw_rectangle', frame)
            k = cv2.waitKey(1) & 0xFF
            if k == ord('q'):
                cv2.destroyAllWindows()
                break
        break


track_colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0),
                (0, 255, 255), (255, 0, 255), (255, 127, 255),
                (127, 0, 255), (127, 0, 127)]


class Shop_Monitor(object):

    def __init__(self, detector_name, tracker_name, config_path='storage/config.cfg'):
        self.det = Detector(detector_name, config_path)
        self.detector_name = detector_name
        self.tra = Tracker_temp(tracker_name, config_path)
        self.config_path = config_path
        self.polygon = []

    def tracking_by_detection(self, video_stream, output_folder=None, show_image=True, detect_freq=2,
                              down_sample_ratio=1.0,
                              is_probability_driven_detect=True, trace=False, heatmap_path=None, is_drawing=False,
                              ratio_filter=1.73, confidence_increation=0.0, location_variation=0.0,
                              video_frame=None, size_display=None):
        video_capture = cv2.VideoCapture(video_stream)
        # get_crop_size(video_capture, down_sample_ratio)
        # self.polygon = points
        # print(self.polygon)
        if is_drawing or readpoints('./storage/human_monitoring.txt') is None:
            get_crop_size(video_capture, down_sample_ratio)
            self.polygon = points
            savefile(self.polygon, './storage/human_monitoring.txt')
        # print(self.polygon)
        else:
            self.polygon = readpoints('./storage/human_monitoring.txt')

        n = len(self.polygon)
        w = int(video_capture.get(cv2.CAP_PROP_FRAME_WIDTH))
        h = int(video_capture.get(cv2.CAP_PROP_FRAME_HEIGHT))

        # Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
        fourcc = cv2.VideoWriter_fourcc(*"mp4v")
        if output_folder != None:
            writer = cv2.VideoWriter(output_folder + 'human_monitoring_freq_' + str(detect_freq)
                                     + '_by_' + self.detector_name + '.mp4',
                                     fourcc, 20.0, (w, h))
        else:
            writer = None

        accumulated_exposures = np.zeros((h, w), dtype=np.float)

        fps = 0.0
        step_counter = 0
        counter = 0
        first_time_flag = True
        start_time = time.time()
        total_time = time.time()
        frame_index = 1
        last_image = None

        while True:
            ret, frame = video_capture.read()
            if ret != True:
                break
            last_image = frame.copy()
            (h, w) = frame.shape[:2]
            masking = np.zeros((frame.shape[0], frame.shape[1]), dtype=np.float)
            frame_resized = cv2.resize(frame, (int(w * down_sample_ratio), int(h * down_sample_ratio)))
            boxes, scores = [], []
            if (step_counter % detect_freq == 0) or counter == 0 or (is_probability_driven_detect == True
                                                                     and self.tra.is_detection_needed() == True):
                results = self.det.detect_image_frame(frame_resized, to_xywh=True, ratio_filter=1.0)
                boxes = np.array([result[1:5] for result in results])
                for box in boxes:
                    box[0] = max(0, box[0] - location_variation)
                if ratio_filter != 1.0:
                    for box in boxes:
                        box[3] = min(box[3], box[2] * ratio_filter)
                boxes = np.array([box for box in boxes if box[2] < w / 3])
                for box in boxes:
                    if box[1] < 80:
                        box[0] -= 46
                scores = np.array([min(1.0, result[5] + confidence_increation) for result in results])
                self.tra.set_detecion_needed(False)

            tracker, detections = self.tra.start_tracking(frame_resized, boxes, scores)
            # Call the tracking
            cashier_counter = 0
            if (IS_TRACKING_DISPLAY is True):
                for track in tracker.tracks:
                    if track.is_confirmed() and track.time_since_update > 1:
                        continue
                    bbox = track.to_tlbr()
                    output = frame.copy()
                    alpha = 0.3
                    cv2.rectangle(frame, (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)),
                                  (int(bbox[2] / down_sample_ratio), int(bbox[3] / down_sample_ratio)), colors[18],
                                  -1)
                    cv2.addWeighted(frame, alpha, output, 1 - alpha, 0, output)

                    frame = output.copy()

                    cv2.rectangle(frame, (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)),
                                  (int(bbox[2] / down_sample_ratio), int(bbox[3] / down_sample_ratio)), colors[18],
                                  1)

                    cv2.putText(frame, str(track.track_id),
                                (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)), 0, 5e-3 * 100,
                                colors[77], 2)

                    w_, h_ = int((bbox[2] - bbox[0]) / down_sample_ratio), int((bbox[3] - bbox[1]) / down_sample_ratio)
                    centroid = (bbox[0] + bbox[2]) / 2, (bbox[1] + bbox[3]) / 2

                    masking[int(bbox[1] / down_sample_ratio):int(bbox[3] / down_sample_ratio),
                    int(bbox[0] / down_sample_ratio): int(bbox[2] / down_sample_ratio)] += 0.01
                    masking[
                    int(bbox[1] / down_sample_ratio) + int(w_ / 8):int(bbox[3] / down_sample_ratio) - int(w_ / 8),
                    int(bbox[0] / down_sample_ratio) + int(h_ / 8): int(bbox[2] / down_sample_ratio) - int(
                        h_ / 8)] += 0.01
                    masking[
                    int(bbox[1] / down_sample_ratio) + int(w_ / 4):int(bbox[3] / down_sample_ratio) - int(w_ / 4),
                    int(bbox[0] / down_sample_ratio) + int(h_ / 4): int(bbox[2] / down_sample_ratio) - int(
                        h_ / 4)] += 0.01
                    masking[
                    int(bbox[1] / down_sample_ratio) + int(3 * w_ / 8):int(bbox[3] / down_sample_ratio) - int(
                        3 * w_ / 8),
                    int(bbox[0] / down_sample_ratio) + int(3 * h_ / 8): int(bbox[2] / down_sample_ratio) - int(
                        3 * h_ / 8)] += 0.01

                    if is_inside_polygon(self.polygon, centroid) == 1:
                        cashier_counter += 1
                    if trace and len(track.prev_box) >= 2:
                        for i in range(0, len(track.prev_box) - 1):
                            # Draw trace line
                            x1 = (track.prev_box[i][0] + track.prev_box[i][2]) / 2
                            y1 = (track.prev_box[i][1] + track.prev_box[i][3]) / 2
                            x2 = (track.prev_box[i + 1][0] + track.prev_box[i + 1][2]) / 2
                            y2 = (track.prev_box[i + 1][1] + track.prev_box[i + 1][3]) / 2
                            clr = track.track_id % 9
                            cv2.line(frame, (int(x1), int(y1)), (int(x2), int(y2)),
                                     track_colors[clr], 2)

            if (IS_DETECTION_DISPLAY is True):
                for detection in detections:
                    bbox = detection.to_tlbr()
                    cv2.rectangle(frame, (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)),
                                  (int(bbox[2] / down_sample_ratio), int(bbox[3] / down_sample_ratio)), (122, 255, 33),
                                  -1)

            accumulated_exposures = accumulated_exposures + masking

            left = 0
            top = 0
            font = cv2.FONT_HERSHEY_COMPLEX_SMALL
            ret, baseline = cv2.getTextSize('Cashier: ' + str(cashier_counter), fontFace=font, fontScale=1.3,
                                            thickness=1)
            cv2.putText(frame, 'Cashier: ' + str(cashier_counter), (left + 10, top + ret[1] + baseline),
                        font, 0.7, (0, 0, 0), 1, cv2.LINE_AA)

            cv2.putText(frame, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                        (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

            for i in range(len(self.polygon)):
                cv2.line(frame, (int(self.polygon[i][0]), int(self.polygon[i][1])),
                         (int(self.polygon[(i + 1) % n][0]), int(self.polygon[(i + 1) % n][1])), (0, 255, 0), 2)

            counter += 1
            step_counter += 1
            if (step_counter % detect_freq == 0):
                fps = step_counter / (time.time() - start_time)
                step_counter = 0
                start_time = time.time()
                if (first_time_flag is True):
                    step_counter = 0
                    counter = 0
                    total_time = time.time()
                    first_time_flag = False

            cv2.putText(frame, 'FPS:' + str(round(fps, 1)), (left + 130, top + ret[1] + baseline),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

            if (show_image == True):
                cv2.imshow('image', frame)
            if writer:
                writer.write(frame)
            frame_index += 1
            if cv2.waitKey(1) & 0xFF == ord('q'):
                cv2.imwrite('./storage/sample/considering_image.jpg', last_image)
                break

        fps = counter / (time.time() - total_time)
        print('Average FPS:', round(fps, 1))
        print('Total eplased:', round(time.time() - total_time, 2))

        if heatmap_path:
            heatmap = np.uint8(cm.jet(accumulated_exposures) * 255)
            heatmap = cv2.cvtColor(heatmap, cv2.COLOR_RGBA2BGR)
            # cv2.imwrite("heatmap.png", canvas)
            # print(canvas)
            # print(last_image.shape, frame.shape, heatmap.shape)
            # for i in range(len(heatmap)):
            #     for j in range(len(heatmap[i])):
            #         if heatmap[i, j, 0] == 127 and heatmap[i, j, 1] == 0 and heatmap[i, j, 2] == 0:
            #             heatmap[i, j] = [255, 255, 255]

            output = last_image.copy()
            last_image = cv2.addWeighted(heatmap, 0.2, output, 0.8, 0, output)
            cv2.putText(last_image, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                        (10, last_image.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (255, 0, 255), 1)

            last_image = output.copy()
            cv2.putText(last_image, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                        (10, last_image.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (255, 0, 255), 2)
            cv2.imwrite(heatmap_path, last_image)

        video_capture.release()
        cv2.destroyAllWindows()


if __name__ == '__main__':
    detector_name = 'darknet19_fssd'
    tracker_name = 'deep_sort'

    tra_by_det = Shop_Monitor(detector_name, tracker_name)

    tra_by_det.tracking_by_detection(video_stream='./storage/sample/videos/alarm.mp4',
                                     show_image=True, detect_freq=1,
                                     down_sample_ratio=1.0, is_probability_driven_detect=True,
                                     trace=True, ratio_filter=1.0)
    # print(os.path.isfile('./sample/videos/counting.mp4'))

    # print(str(sum([fps * nb_frames for fps, nb_frames in zip(fps_list, nb_frames_list)]) / sum(nb_frames_list)))
