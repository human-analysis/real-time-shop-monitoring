import colorsys
import datetime
import errno
import os
import queue
import threading
import time
import warnings

import cv2
import numpy as np
from matplotlib import cm

from libs.detection.detector import Detector
from libs.geometry.polygon import PolygonDrawer
from libs.tracking.tracker import Tracker_temp
from libs.utils.geometry_common import orientation, get_angle

warnings.filterwarnings('ignore')
IS_DETECTION_DISPLAY = False
IS_TRACKING_DISPLAY = True
points = []

hsv_tuples = [(x / 80, 1., 1.)
              for x in range(80)]
colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
colors = list(
    map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)), colors))


def draw_poly():
    pass


def get_crop_size(cap, down_sample_ratio):
    global points
    pd = PolygonDrawer("draw polygon")
    while cap.isOpened():
        ret, frame = cap.read()
        (h, w) = frame.shape[:2]
        frame = cv2.resize(frame, (int(w * down_sample_ratio), int(h * down_sample_ratio)))
        frame = pd.run(frame)
        points = pd.points
        while 1:
            cv2.imshow('draw_rectangle', frame)
            k = cv2.waitKey(1) & 0xFF
            if k == ord('q'):
                cv2.destroyAllWindows()
                break
        break


track_colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0),
                (0, 255, 255), (255, 0, 255), (255, 127, 255),
                (127, 0, 255), (127, 0, 127)]


class Shop_Monitoring(object):

    def __init__(self, detector_name, tracker_name, config_path='storage/config.cfg'):
        self.det = Detector(detector_name, config_path)
        self.detector_name = detector_name
        self.tra = Tracker_temp(tracker_name, config_path)
        self.config_path = config_path
        self.polygon = []
        self.running = True
        self.q = queue.Queue()

    # def new_tk_image(selqf, frame, size):
    #     image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    #     image = Image.fromarray(image).resize(size)
    #     imageTk = ImageTk.PhotoImage(image=image)
    #     return imageTk

    def open_with_mkdir(self, path):
        try:
            os.makedirs(os.path.dirname(path))
        except OSError as e:
            if e.errno == errno.EEXIST and os.path.isdir(os.path.dirname(path)):
                pass
            else:
                raise

        return open(path, 'w')

    def grab(self, cam, queue, delay):

        capture = cv2.VideoCapture(cam)
        now = time.time()

        if type(cam) == int:
            while self.running:
                fr = {}
                capture.grab()
                retval, img = capture.read()
                if not retval:
                    self.running = False
                fr["img"] = img
        else:
            while self.running:
                fr = {}
                retval, img = capture.read()
                if not retval:
                    self.running = False
                fr["img"] = img
                cur = time.time()
                if cur - now >= delay:
                    queue.put(fr)
                    now = cur

    def tracking_by_detection(self, video_stream, show_image=True, detect_freq=2, down_sample_ratio=1.0,
                              is_probability_driven_detect=True, trace=False,
                              ratio_filter=1.73, confidence_increation=0.0, location_variation=0.0,
                              video_frame=None, size_display=None):
        video_capture = cv2.VideoCapture(video_stream)
        # get_crop_size(video_capture, down_sample_ratio)
        self.polygon = points
        # print(self.polygon)
        # ix, iy, ex, ey = points[0][0], points[0][1], points[1][0], points[1][1]
        # ix1, iy1, ex1, ey1 = points[-1][0], points[-1][1], points[-2][0], points[-2][1]

        # ix, iy, ex, ey = 23, 172, 620, 168
        # ix1, iy1, ex1, ey1 = 23, 235, 619, 232

        ix, iy, ex, ey = 318, 428, 468, 719
        ix1, iy1, ex1, ey1 = 252, 454, 370, 720
        w = int(video_capture.get(cv2.CAP_PROP_FRAME_WIDTH))
        h = int(video_capture.get(cv2.CAP_PROP_FRAME_HEIGHT))

        # Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
        fourcc = cv2.VideoWriter_fourcc(*"mp4v")
        video_fps = video_capture.get(cv2.CAP_PROP_FPS)
        writer = cv2.VideoWriter('./experiments/videos/full_thread_human_counting_freq_' + str(
            detect_freq) + '_by_' + self.detector_name + '.mp4',
                                 fourcc, video_fps, (w, h))

        accumulated_exposures = np.zeros((h, w), dtype=np.float)

        start_time = time.time()
        total_time = time.time()
        step_counter = 0
        counter = 0
        first_time_flag = True
        textIn = 0
        textOut = 0
        last_image = None

        capture_thread = threading.Thread(target=self.grab, args=(video_stream, self.q, 0))
        capture_thread.start()

        result_list = []
        frame_index = 1
        fps = 0
        while True:
            try:
                fr = self.q.get()
                frame = fr['img']
                (h, w) = frame.shape[:2]
                last_image = frame
                masking = np.zeros((frame.shape[0], frame.shape[1]), dtype=np.float)
                frame_resized = cv2.resize(frame, (int(w * down_sample_ratio), int(h * down_sample_ratio)))

                if (step_counter % detect_freq == 0) or counter == 0 or (is_probability_driven_detect == True
                                                                         and self.tra.is_detection_needed() == True):
                    results = self.det.detect_image_frame(frame_resized, to_xywh=True, ratio_filter=1.0)
                    boxes = np.array([result[1:5] for result in results])
                    for box in boxes:
                        box[0] = max(0, box[0] - location_variation)
                    if ratio_filter != 1.0:
                        for box in boxes:
                            box[3] = min(box[3], box[2] * ratio_filter)
                    boxes = np.array([box for box in boxes if box[2] < w / 3])
                    scores = np.array([min(1.0, result[5] + confidence_increation) for result in results])
                    self.tra.set_detecion_needed(False)

                tracker, detections = self.tra.start_tracking(frame_resized, boxes, scores)
                # Call the tracking
                if (IS_TRACKING_DISPLAY is True):
                    for track in tracker.tracks:
                        if track.is_confirmed() and track.time_since_update > 1:
                            continue
                        bbox = track.to_tlbr()
                        w_, h_ = int((bbox[2] - bbox[0]) / down_sample_ratio), int(
                            (bbox[3] - bbox[1]) / down_sample_ratio)

                        output = frame.copy()
                        alpha = 0.3
                        cv2.rectangle(frame, (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)),
                                      (int(bbox[2] / down_sample_ratio), int(bbox[3] / down_sample_ratio)), colors[18],
                                      -1)
                        cv2.addWeighted(frame, alpha, output, 1 - alpha, 0, output)
                        frame = output.copy()
                        cv2.rectangle(frame, (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)),
                                      (int(bbox[2] / down_sample_ratio), int(bbox[3] / down_sample_ratio)), colors[18],
                                      1)
                        cv2.putText(frame, str(track.track_id),
                                    (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)), 0, 5e-3 * 80,
                                    (0, 255, 0), 2)

                        # bbox = track.to_tlwh()
                        centroid = (bbox[0] + bbox[2]) / 2, (bbox[1] + bbox[3]) / 2
                        if trace and len(track.prev_box) >= 2:
                            for i in range(0, len(track.prev_box) - 1):
                                # Draw trace line
                                x1 = (track.prev_box[i][0] + track.prev_box[i][2]) / 2
                                y1 = (track.prev_box[i][1] + track.prev_box[i][3]) / 2
                                x2 = (track.prev_box[i + 1][0] + track.prev_box[i + 1][2]) / 2
                                y2 = (track.prev_box[i + 1][1] + track.prev_box[i + 1][3]) / 2
                                clr = track.track_id % 9
                                cv2.line(frame, (int(x1), int(y1)), (int(x2), int(y2)),
                                         track_colors[clr], 2)

                        masking[int(bbox[1] / down_sample_ratio):int(bbox[3] / down_sample_ratio),
                        int(bbox[0] / down_sample_ratio): int(bbox[2] / down_sample_ratio)] += 0.01
                        masking[
                        int(bbox[1] / down_sample_ratio) + int(w_ / 8):int(bbox[3] / down_sample_ratio) - int(w_ / 8),
                        int(bbox[0] / down_sample_ratio) + int(h_ / 8): int(bbox[2] / down_sample_ratio) - int(
                            h_ / 8)] += 0.01
                        masking[
                        int(bbox[1] / down_sample_ratio) + int(w_ / 4):int(bbox[3] / down_sample_ratio) - int(w_ / 4),
                        int(bbox[0] / down_sample_ratio) + int(h_ / 4): int(bbox[2] / down_sample_ratio) - int(
                            h_ / 4)] += 0.01
                        masking[
                        int(bbox[1] / down_sample_ratio) + int(3 * w_ / 8):int(bbox[3] / down_sample_ratio) - int(
                            3 * w_ / 8),
                        int(bbox[0] / down_sample_ratio) + int(3 * h_ / 8): int(bbox[2] / down_sample_ratio) - int(
                            3 * h_ / 8)] += 0.01

                        if len(track.to_centroids()):
                            x = [c[0] for c in track.to_centroids()]
                            y = [c[1] for c in track.to_centroids()]
                            orientation_ = centroid[1] - np.mean(y) - 0

                            perp_vec = (iy - ey, ex - ix)
                            direc_vec = (centroid[0] - np.mean(x), centroid[1] - np.mean(y))

                            direc_angle = get_angle(perp_vec, direc_vec)

                            # print(orientation, centroid, ey)
                            if not track.counted:
                                temp1 = orientation((ix, iy), (ex, ey), centroid)
                                temp2 = orientation((ix1, iy1), (ex1, ey1), centroid)
                                if direc_angle > 150 and temp1 == 2 and temp2 == 1:
                                    textIn += 1
                                    track.counted = True
                                elif direc_angle < 95 and temp1 == 2 and temp2 == 1:
                                    textOut += 1
                                    track.counted = True

                accumulated_exposures = accumulated_exposures + masking

                counter += 1
                step_counter += 1
                if (step_counter % detect_freq == 0):
                    fps = step_counter / (time.time() - start_time)
                    step_counter = 0
                    start_time = time.time()
                    if (first_time_flag is True):
                        step_counter = 0
                        counter = 0
                        total_time = time.time()
                        first_time_flag = False

                if IS_DETECTION_DISPLAY is True:
                    for detection in detections:
                        bbox = detection.to_tlbr()
                        output = frame.copy()
                        alpha = 0.3
                        cv2.rectangle(frame, (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)),
                                      (int(bbox[2] / down_sample_ratio), int(bbox[3] / down_sample_ratio)), colors[18],
                                      -1)
                        cv2.addWeighted(frame, alpha, output, 1 - alpha, 0, output)
                        frame = output.copy()
                        cv2.rectangle(frame, (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)),
                                      (int(bbox[2] / down_sample_ratio), int(bbox[3] / down_sample_ratio)), colors[18],
                                      1)

                cv2.putText(frame, 'FPS:' + str(round(fps, 1)), (0, 100), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                font = cv2.FONT_HERSHEY_COMPLEX_SMALL
                cv2.line(frame, (int(ix), int(iy)), (int(ex), int(ey)), (0, 255, 0), 2)
                cv2.line(frame, (int(ix1), int(iy1)), (int(ex1), int(ey1)), (0, 255, 0), 2)

                left = 0
                top = 0
                ret, baseline = cv2.getTextSize('people out: ' + str(textOut), fontFace=font, fontScale=1.3,
                                                thickness=1)
                cv2.rectangle(frame, (left, top), (left + ret[0], top + ret[1] + baseline),
                              color=(122, 255, 33), thickness=-1)
                cv2.putText(frame, 'people out: ' + str(textOut), (left, top + ret[1] + baseline), font, 0.7, (0, 0, 0),
                            1,
                            cv2.LINE_AA)

                text = 'people in: ' + str(textIn)
                left = 0
                top = 30
                cv2.rectangle(frame, (left, top), (left + ret[0], top + ret[1] + baseline),
                              color=(255, 255, 255), thickness=-1)
                cv2.putText(frame, text, (left, top + ret[1] + baseline), font, 0.7, (0, 0, 0), 1, cv2.LINE_AA)
                cv2.putText(frame, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                            (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

                writer.write(frame)
                frame_index += 1
                if (show_image == True):
                    cv2.imshow('image', frame)
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break

            except Exception as inst:
                print(inst)
                self.running = False
                break

        fps = counter / (time.time() - total_time)
        print('Average FPS:', round(fps, 1))
        print('Total eplased:', round(time.time() - total_time, 2))

        if True:
            heatmap = np.uint8(cm.jet(accumulated_exposures) * 255)
            heatmap = cv2.cvtColor(heatmap, cv2.COLOR_RGBA2BGR)
            # cv2.imwrite("heatmap.png", canvas)
            # print(canvas)
            # print(last_image.shape, frame.shape, heatmap.shape)
            # for i in range(len(heatmap)):
            #     for j in range(len(heatmap[i])):
            #         if heatmap[i, j, 0] == 127 and heatmap[i, j, 1] == 0 and heatmap[i, j, 2] == 0:
            #             heatmap[i, j] = [255, 255, 255]

            output = last_image.copy()
            last_image = cv2.addWeighted(heatmap, 0.2, output, 0.8, 0, output)
            cv2.putText(last_image, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                        (10, last_image.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (255, 0, 255), 1)

            last_image = output.copy()
            cv2.putText(last_image, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                        (10, last_image.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (255, 0, 255), 2)
            cv2.imwrite('./experiments/images/heat_map.png', last_image)

        writer.release()
        video_capture.release()
        cv2.destroyAllWindows()


if __name__ == '__main__':
    detector_name = 'darknet19_fssd'
    tracker_name = 'deep_sort'

    tra_by_det = Shop_Monitoring(detector_name, tracker_name)

    tra_by_det.tracking_by_detection(video_stream='./storage/sample/videos/cropped_counting1.mp4',
                                     show_image=False, detect_freq=1, trace=True, ratio_filter=1.0,
                                     down_sample_ratio=1.0, is_probability_driven_detect=True)
    # print(os.path.isfile('./sample/videos/counting.mp4'))

    # print(str(sum([fps * nb_frames for fps, nb_frames in zip(fps_list, nb_frames_list)]) / sum(nb_frames_list)))
