import configparser

from ..tracker_template import Tracker_Template


class NonDeep_Tracker(Tracker_Template):
    def __init__(self, config_path):
        super(NonDeep_Tracker, self).__init__()
        config = configparser.ConfigParser()
        config.read(config_path)

        self.sigma_l = config.get('non_deep', 'sigma_l')
        self.sigma_h = config.get('non_deep', 'sigma_h')
        self.sigma_iou = config.get('non_deep', 'sigma_iou')
        self.t_min = config.get('non_deep', 't_min')
        self.tracks_active = []
        self.tracks_finished = []

    def start_tracking(self, frame, boxes, scores):
        dets = []
        for idx, det in enumerate(boxes):
            if scores[idx] >= self.sigma_l:
                dets.append(det)
        updated_tracks = []
        for track in self.tracks_active:
            if len(dets) > 0:
                best_match = max(dets, key=lambda x: iou(track['boxes'][-1], x['bbox']))
                if iou(track['bboxes'][-1], best_match['bbox']) >= self.sigma_iou:
                    track['bboxes'].append(best_match['bbox'])
                    track['max_score'] = max(track['max_score'], best_match['score'])

                    updated_tracks.append(track)

                    # remove from best matching detection from detections
                    del dets[dets.index(best_match)]

                    # if track was not updated
            if len(updated_tracks) == 0 or track is not updated_tracks[-1]:
                # finish track when the conditions are met
                if track['max_score'] >= self.sigma_h and len(track['bboxes']) >= self.t_min:
                    self.tracks_finished.append(track)
        # create new tracks
        new_tracks = [{'bboxes': [det['bbox']], 'max_score': det['score'], 'start_frame': frame} for det in
                      dets]
        self.tracks_active = updated_tracks + new_tracks
        return None, None

    def is_detection_needed(self):
        pass
