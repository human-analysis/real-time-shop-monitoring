# -*- coding: utf-8 -*-
"""
Created on Sun Jun 5 14:00:00 2019

@author: Tuanhleo
"""

from .models.deepsort_tracker import Deepsort_Tracker
from .tracker_template import Tracker_Template


class Tracker_temp(Tracker_Template):
    def __init__(self, tracker_name, config_path):
        super(Tracker_Template, self).__init__()
        self.tracker = self._tracker_selection(tracker_name, config_path)

    @staticmethod
    def _tracker_selection(tracker_name, config_path):
        tracker_map = {
            'deep_sort': Deepsort_Tracker
        }

        return tracker_map[tracker_name](config_path)

    def start_tracking(self, frame, boxes, scores):
        return self.tracker.start_tracking(frame, boxes, scores)

    def need_detection(self):
        return self.tracker.need_detection()

    def set_need_detection(self, value):
        self.tracker.set_need_detection(value)

    def is_detection_needed(self):
        return self.tracker.is_detection_needed()

    def set_detecion_needed(self, value):
        self.tracker.set_detecion_needed(value)
