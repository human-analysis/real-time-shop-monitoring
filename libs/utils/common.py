import argparse
import os


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def readpoints(file):
    if os.path.isfile(file):
        points = []
        with open(file, "r") as f:
            lines = f.readlines()
            for line in lines:
                points.append([int(x) for x in line.strip('\n').split(',')])
            return points
    else:
        return None


def savefile(points, file):
    with open(file, 'w') as f:
        for idx, point in enumerate(points):
            if idx == len(points) - 1:
                temp = ''
            else:
                temp = '\n'
            f.write(str(point).lstrip('(').rstrip(')') + temp)
