import math


def onSegment(p, q, r):
    if max(p[0], r[0]) >= q[0] >= min(p[0], r[0]):
        if max(p[1], r[1]) >= q[1] >= min(p[1], r[1]):
            return True
    return False


def orientation(p, q, r):
    val = (q[1] - p[1]) * (r[0] - q[0]) - (q[0] - p[0]) * (r[1] - q[1])
    if val == 0: return 0
    if val > 0: return 1
    return 2


def do_intersect(p1, q1, p2, q2):
    o1 = orientation(p1, q1, p2)
    o2 = orientation(p1, q1, q2)
    o3 = orientation(p2, q2, p1)
    o4 = orientation(p2, q2, q1)

    if (o1 != o2 and o3 != o4):
        return True
    if (o1 == 0 and onSegment(p1, p2, q1)):
        return True
    if (o2 == 0 and onSegment(p1, q2, q1)):
        return True
    if (o3 == 0 and onSegment(p2, p1, q2)):
        return True
    if (o4 == 0 and onSegment(p2, q1, q2)):
        return True
    return False


def is_inside_polygon(polygon, p):
    n = len(polygon)
    if n < 3:
        return False
    extreme = [-99999999, p[1]]
    count, i = 0, 0
    while True:
        next = (i + 1) % n
        if do_intersect(polygon[i], polygon[next], p, extreme):
            if orientation(polygon[i], p, polygon[next]) == 0:
                return onSegment(polygon[i], p, polygon[next])
            count += 1
        i = next
        if i == 0:
            break
    return count & 1


def get_angle(v1, v2):
    d = (v1[0] * v2[0] + v1[1] * v2[1])
    e1 = math.sqrt(v1[0] * v1[0] + v1[1] * v1[1])
    e2 = math.sqrt(v2[0] * v2[0] + v2[1] * v2[1])

    d = d / (e1 * e2)
    pi = 3.14159
    return (180 / pi) * math.acos(d)


def get_intersect(A, B, C, D):
    a1 = B[1] - A[1]
    b1 = A[0] - B[0]
    c1 = a1 * (A[0]) + b1 * (A[1])

    a2 = D[1] - C[1]
    b2 = C[0] - D[0]
    c2 = a2 * (C[0]) + b2 * (C[1])

    determinant = a1 * b2 - a2 * b1

    if determinant == 0:
        return None
    else:
        x = (b2 * c1 - b1 * c2) / determinant
        y = (a1 * c2 - a2 * c1) / determinant
        return [x, y]
