import os

from setuptools import setup
from torch.utils.cpp_extension import BuildExtension, CUDAExtension

thisdir = os.path.dirname(os.path.abspath(__file__))
setup(
    name='cuda_extension',
    ext_modules=[
        CUDAExtension(
            name='libs.utils.pytorch_nms.nms',
            sources=[thisdir + '/nms.cpp', thisdir + '/nms_kernel.cu'],
            extra_compile_args={'cxx': ['-g'],
                                'nvcc': ['-O2']})
    ],
    cmdclass={
        'build_ext': BuildExtension
    })
