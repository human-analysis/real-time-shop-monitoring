# import onnxruntime as rt
import time
import numpy
import os
from glob import glob
from libs.utils.dataset import preproc_for_test, my_preproc_for_test
import onnx
from caffe2.python.onnx.backend import Caffe2Backend as c2
import cv2
from libs.utils.fssd_utils.config_parse import cfg, cfg_from_file
from libs.detection.models.fssd_modeling.layers.functions.prior_box import PriorBox
from libs.detection.models.fssd_modeling.layers.functions.detection import Detect
from libs.detection.models.fssd_modeling.model_builder import create_model
import torch
from torch.autograd import Variable


import onnx_caffe2.backend as backend
import numpy as np


# def original_onnx_testing(onnx_path="./storage/onnx_models/fssd_darknet19.onnx"):
#     image_samples = glob(os.path.join('./storage/sample/images', '*.jpg'))
#     # sess = rt.InferenceSession(onnx_path)
#     # model = onnx.load("./storage/onnx_models/mobilenetv1_fssd.onnx")
#     # rep = c2.prepare(model)
#     # rep.predict_net.type = 'prof_dacg'
#     input_name = sess.get_inputs()[0].name
#     targets = sess.get_outputs()[0].name
#     labels = sess.get_outputs()[1].name
#     total_time = 0
#     for img_path in image_samples:
#         img_ = cv2.imread(img_path)
#         start = time.time()
#         img = my_preproc_for_test(img_,(300, 300), (103.94, 116.78, 123.68))
#         pred_onx = sess.run([targets, labels], {input_name: img.astype(numpy.float32)})
#         # outputs = rep.run(img)
#         total_time += time.time() - start
#
#     print(total_time)
def model_preprocessing():
    cfg_from_file('./storage/cfgs/fssd_lite_mobilenetv1_train_smartshop.yml')
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    _, priorbox = create_model(cfg.MODEL)
    with torch.no_grad():
        priors = Variable(priorbox.forward())

    use_gpu = torch.cuda.is_available()
    half = False
    if use_gpu:
        print('Utilize GPUs for computation')
        print('Number of GPU avaiable', torch.cuda.device_count())
        priors = priors.to(device)
        if half:
            priors = priors.half()
    return priors

def caffe2_testing(onnx_path="./storage/onnx_models/fssd_lite_mobilenetv1.onnx"):
    image_samples = glob(os.path.join('./storage/sample/images', '*.jpg'))
    model = onnx.load(onnx_path)
    priors = model_preprocessing()
    detector = Detect(cfg.POST_PROCESS, priors)
    # Check that the IR is well formed
    onnx.checker.check_model(model)

    # Print a human readable representation of the graph
    onnx.helper.printable_graph(model.graph)

    rep = c2.prepare(model, device="CUDA:0")
    # rep = backend.prepare(model)
    rep.predict_net.type = 'prof_dag'
    total_time = 0
    for img_path in image_samples:
        img_ = cv2.imread(img_path)
        start = time.time()
        img = my_preproc_for_test(img_,(300, 300), (103.94, 116.78, 123.68))
        outputs = rep.run(img)
        # print(outputs._0)
        detections = detector.forward((torch.tensor(outputs._0), torch.tensor(outputs._1)))
        # outputs = rep.run(img)
        total_time += time.time() - start

    print(total_time/len(image_samples))


if __name__ == "__main__":
    caffe2_testing()