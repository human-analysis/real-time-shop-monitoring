import argparse
from libs.human_counting import Shop_Counter
from libs.human_monitoring import Shop_Monitor
from libs.utils.common import str2bool

functions = {
    "counting": Shop_Counter,
    "monitoring": Shop_Monitor
}

def main():
    args = argparser.parse_args()
    detector = args.detector
    tracker = args.tracker
    is_drawing = args.draw
    function = args.option
    is_show = args.show
    input = args.input
    output_folder = None if args.output_folder == '' else args.output_folder
    heatmap_output = args.heatmap_output

    tra_by_det = functions[function](detector, tracker)
    tra_by_det.tracking_by_detection(input, output_folder=output_folder, show_image=is_show, detect_freq=args.detect_freq,
                            down_sample_ratio=args.down_sample_ratio, is_probability_driven_detect=args.is_probability_driven_detect,
                            trace=args.trace, is_drawing=is_drawing, heatmap_path=heatmap_output, ratio_filter=args.ratio_filter,
                            confidence_increation=args.confidence_increation, location_variation=0.0, video_frame=None, size_display=None)


if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("--draw", default=False, type=str2bool)
    argparser.add_argument("--detect_freq", default=2, type=int)
    argparser.add_argument("--down_sample_ratio", default=1.0, type=float)
    argparser.add_argument("--ratio_filter", default=1.73, type=float)
    argparser.add_argument("--confidence_increation", default=0.0, type=float)
    argparser.add_argument("--is_probability_driven_detect", default=True, type=str2bool)
    argparser.add_argument("--trace", default=False, type=str2bool)
    argparser.add_argument("--detector", default="darknet19_fssd", type=str)
    argparser.add_argument("--tracker", default="deep_sort", type=str)
    argparser.add_argument("--thread", default=False, type=str2bool)
    argparser.add_argument("--option", default="counting", type=str)
    argparser.add_argument("--input", default='./storage/sample/videos/cropped_counting1.mp4', type=str)
    argparser.add_argument("--show", default=True, type=str2bool)
    argparser.add_argument("--heatmap_output", type=str)
    argparser.add_argument("--output_folder", default='', type=str)
    main()
