#!/usr/bin/env bash
set -e
set -o pipefail

if [ -z $1 ]
then
  echo "missing run name"
  exit 1
fi

function install_dependencies() {
  apt-get update >/dev/null
  apt-get -y install python3-dev libsm-dev libxrender1 libxext6 zip git >/dev/null
  rm -rf /var/lib/apt/lists/*

  pip -q install virtualenv
  virtualenv env --python=python3
  . env/bin/activate

  pip -q install -r storage/requirements.txt
}

#RUN_NAME=$1
#
#while (( "$#" ))
#do
#  case "$1" in
#  --)
#    shift
#    break
#    ;;
#  *)
#    shift
#    ;;
#  esac
#done
install_dependencies

