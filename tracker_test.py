from libs.tracking.tracker import Tracker_temp

if __name__ == "__main__":
    tracker = Tracker_temp(tracker_name='deep_sort', config_path='./storage/config.cfg')
    tracker.start_tracking()